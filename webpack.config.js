const {resolve} = require("path");
var BrowserSyncPlugin = require('browser-sync-webpack-plugin');

module.exports = {

    entry: {
        mquery: "./src/js/mQuery/mQuery.js",
        //polyfill: "babel-polyfill"
    },

    output: {
        path: resolve(__dirname + "/src/dist/js"),
        filename: "[name].js",
        library: "mQuery",
        libraryTarget: "umd",
        umdNamedDefine: true
    },

    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader",
                    options: {
                        plugins: ["transform-runtime", "add-module-exports"],
                        presets: ["es2015"]
                    }
                }
            }
        ]
    },

    plugins: [
        new BrowserSyncPlugin({
            host: 'localhost',
            port: 3000,
            server: {baseDir: ['src']},
            files: ['./src/index.html', './src/js/script.js']
        })
    ]
};