function AJAX(config) {

    if (!(this instanceof AJAX)) {
        return new AJAX(config);
    }

    this._xhr = new XMLHttpRequest();
    this._config = this._extendOptions(config);

    this._beforeSend();

};


AJAX.prototype._defaultConfig = {
    type: 'GET',
    url: window.location.href,
    data: {},
    headers: {},
    options: {
        async: true,
        timeout: 0,
        username: null,
        password: null,
        defaultHeaders: true
    }
};


AJAX.prototype._beforeSend = function () {

    this._assignEvents();
    this._open();
    this._assignUserHeaders();
    this._send();

};


AJAX.prototype._extendOptions = function (config) {

    function mergeObject(origin, mixin, deeps) {

        if (mixin) {
            if (!Array.isArray(deeps)) {
                deeps = [];
            }

            for (var key in origin) {
                if (deeps.indexOf(key) < 0 && key in mixin) {
                    origin[key] = mixin[key];
                } else {
                    origin[key] = mergeObject(origin[key], mixin[key], deeps);
                }
            }
        }

        return origin;
    }

    var options = JSON.parse(JSON.stringify(this._defaultConfig));
    options = mergeObject(options, config, ['options']);

    options.success = config.success && typeof config.success === 'function' ? config.success : function () {
    };
    options.failure = config.failure && typeof config.failure === 'function' ? config.failure : function () {
    };

    if (options.type === 'GET') {
        options.url += this._serializeGetData(options);
    }

    return options;
};


AJAX.prototype._assignEvents = function () {

    this._xhr.addEventListener('readystatechange', this._handleResponse.bind(this), false);
    this._xhr.addEventListener('timeout', this._handleError.bind(this), false);
    this._xhr.addEventListener('abort', this._handleError.bind(this), false);
    this._xhr.addEventListener('error', this._handleError.bind(this), false);

};


AJAX.prototype._open = function () {

    this._xhr.open(
        this._config.type,
        this._config.url,
        this._config.options.async,
        this._config.options.username,
        this._config.options.password,
    );

    this._xhr.timeout = this._config.options.timeout;

};

AJAX.prototype._serializeGetData = function (options) {

    if (typeof options === 'object') {

        var request = '?';

        for (var data in options.data) {
            request += decodeURI(data) + '=' + decodeURI(options.data[data]) + '&';
        }

        return request.slice(0, -1);

    } else if (typeof options === 'string') {

        return '?' + decodeURI(options);

    }

};

AJAX.prototype._serializePostData = function () {

    if (this._config.type.toUpperCase() === 'POST') {

        if (typeof this._config.data === 'object') {

            var formData = new FormData();

            for (var data in this._config.data) {
                formData.append(data, this._config.data[data]);
            }

            return formData
        }

        if (typeof this._config.data === 'string') {
            return JSON.stringify(this._config.data);
        }

    } else if (this._config.type.toUpperCase() === 'GET') {

        return null;

    }

};


AJAX.prototype._send = function () {

    this._xhr.send(this._serializePostData());

};


AJAX.prototype._assignUserHeaders = function () {

    if (this._config.options.defaultHeaders) {
        this._xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
    }
    for (var header in this._config.headers) {
        this._xhr.setRequestHeader(header, this._config.headers[header]);
    }

};


AJAX.prototype._handleResponse = function () {

    if (this._xhr.readyState === 4 && this._xhr.status >= 200 && this._xhr.status < 400) {
        this._config.success(this._xhr.response, this._xhr);
    } else if (this._xhr.readyState === 4 && this._xhr.status >= 400) {
        this._handleError();
    }

};


AJAX.prototype._handleError = function () {

    this._config.failure(this._xhr);

};


window.AJAX = AJAX;
/*
AJAX({
    type: 'POST',
    url: 'http://rekrutacja-webhosting.it.krd.pl/api/Recruitment/GetFilteredDebts',
    data: "112",
    headers: {
        "Content-type": "application/json"
    },
    options: {
        defaultHeaders: false
    },
    success: function (response, xhr) {
        console.log('response ', response);
        console.log('xhr: ', xhr);
    },
    failure: function (xhr) {
        console.log('xhr: ', xhr);
    }
});*/
/*

AJAX({
    type: 'GET',
    url: 'http://rekrutacja-webhosting.it.krd.pl/api/Recruitment/GetTopDebts',
    data: {
        'dept': 12,
        'cos': 'asd ads'
    },
    success: function (response, xhr) {
        console.log('response ', response);
        console.log('xhr: ', xhr);
    },
    failure: function (xhr) {
        console.log('xhr: ', xhr);
    }
});*/
