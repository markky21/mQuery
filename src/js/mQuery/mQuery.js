import mQuery from "./Library/mQuery"
import {staticMixin} from "./Utils/Mixin";
import {isDOMNode, isHTMLTag, isSelector} from "./Utils/Check";
import {findElements, createElement} from "./Utils/Element";

import Ajax from "./Library/Ajax";


function init(param) {

    let nodes = null;

    if (isSelector(param)) {
        // console.log('To jest string');
        nodes = findElements(param);
    } else if (isHTMLTag(param)) {
        // console.log('To jest < >');
        nodes = createElement(param);
    } else if (isDOMNode(param)) {
        // console.log('To jest Element');
        nodes = param;
    }

    return mQuery.create(nodes);
}

staticMixin(init, Ajax);

export default init;