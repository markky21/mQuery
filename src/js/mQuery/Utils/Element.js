export function findElements (val) {

    return Array.from(document.querySelectorAll(val));

}

export function createElement (val) {

    const match = /^<(\w+)>$/.exec(val)[1];
    return document.createElement(match);

}
