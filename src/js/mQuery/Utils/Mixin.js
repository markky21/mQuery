export function mixin(...mixins) {
    function fn() {
    };

    Object.assign(fn.prototype, ...mixins);

    return fn;
}

export function staticMixin(objToMix, ...mixins) {

    Object.assign(objToMix, ...mixins);

    return objToMix;

}