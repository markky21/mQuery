class Ajax {

    constructor(type = 'GET', url = window.location.href, options = {}) {

        this._config = {
            type: type,
            url: url,
            data: (options.data ? options.data : {}),
            headers: (options.headers ? options.headers : {}),
            options: {
                async: (options.async === false ? false : true),
                timeout: (options.timeout ? options.timeout : 0),
                username: (options.username ? options.username : null),
                password: (options.password ? options.password : null),
                defaultHeaders: (options.defaultHeaders === false ? false : true),
            },
            success: (options.success ? options.success : () => {
            }),
            failure: (options.failure ? options.failure : () => {
            })
        };

        this._xhr = new XMLHttpRequest();

        return this._beforeSend();
    }

    _beforeSend() {

        if (this._config.type === 'GET') {
            this._config.url += this._serializeGetData(this._config.data);
        }

        let promise = this._assignEvents();
        this._open();
        this._assignUserHeaders();
        this._send();

        return promise;
    };

    _assignEvents() {

        let promise = new Promise((resolve, reject) => {

            this._xhr.addEventListener('readystatechange', this._handleResponse.bind(this, resolve, reject), false);
            this._xhr.addEventListener('timeout', this._handleError.bind(this, reject), false);
            this._xhr.addEventListener('abort', this._handleError.bind(this, reject), false);
            this._xhr.addEventListener('error', this._handleError.bind(this, reject), false);

        });

        return promise

    };

    _open() {

        this._xhr.open(
            this._config.type,
            this._config.url,
            this._config.options.async,
            this._config.options.username,
            this._config.options.password,
        );

        this._xhr.timeout = this._config.options.timeout;

    };

    _assignUserHeaders() {

        if (this._config.options.defaultHeaders) {
            this._xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
        }

        for (let header in this._config.headers) {
            this._xhr.setRequestHeader(header, this._config.headers[header]);
        }

    };

    _send() {

        this._xhr.send(this._serializePostData());

    };

    _handleResponse(resolve, reject) {

        if (this._xhr.readyState === 4 && this._xhr.status >= 200 && this._xhr.status < 400) {
            this._config.success(this._xhr.response, this._xhr);
            resolve(this._xhr.response);

        } else if (this._xhr.readyState === 4 && this._xhr.status >= 400) {
            this._handleError(reject);
        }

    };

    _handleError(reject) {

        this._config.failure(this._xhr);
        reject(new Error(`Ajax error! Status code: ${this._xhr.status}`));

    };

    _serializePostData() {

        if (this._config.type.toUpperCase() === 'POST') {

            if (typeof this._config.data === 'object') {

                const formData = new FormData();

                for (let data in this._config.data) {
                    formData.append(data, this._config.data[data]);
                }

                return formData
            }

            if (typeof this._config.data === 'string') {
                return JSON.stringify(this._config.data);
            }

        } else if (this._config.type.toUpperCase() === 'GET') {

            return null;

        }

    }

    _serializeGetData(options) {

        if (typeof options === 'object') {

            let request = '?';

            for (let data in options.data) {
                request += decodeURI(data) + '=' + decodeURI(options.data[data]) + '&';
            }

            return request.slice(0, -1);

        } else if (typeof options === 'string') {

            return '?' + decodeURI(options);

        }


    }

}

export default Ajax;
