export function isSelector(param) {

    return (
        typeof param === 'string' &&
        !isHTMLTag(param) &&
        !isDOMNode(param)
    );
}

export function isHTMLTag(param) {
    return (
        typeof param === 'string' &&
        param.search(/^<\w+>$/) === 0)
}

export function isDOMNode(param) {
    return (
        typeof param === "object" &&
        param instanceof Element
    )
}

