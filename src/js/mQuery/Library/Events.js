export default {

    on(type, fn, useCapture = false) {
        return this.each(node => {

            node.addEventListener(type, fn, useCapture);

        });
    },

    off(type, fn, useCapture = false) {

        return this.each(node => {

            node.removeEventListener(type, fn, useCapture);

        });

    }

}