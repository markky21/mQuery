import mQuery from "./mQuery";

export default {


    find(query) {
        return mQuery.create(this.get(0).querySelectorAll(query));
    },


    remove() {
        const parent = this.get(0).parentElement;
        parent.removeChild(this.get(0));
        return mQuery.create(parent);
    },


    parent() {
        return mQuery.create(this.get(0).parentElement);
    },


    closest(selector) {

        let element = this.get(0),
        matchesFn,
        parent;

        ['matches', 'webkitMatchesSelector', 'mozMatchesSelector', 'msMatchesSelector', 'oMatchesSelector'].some(function (fn) {
            if (typeof document.body[fn] === 'function') {
                matchesFn = fn;
                return true;
            }
            return false;
        });

        while (element) {
            parent = element.parentElement;
            if (parent && parent[matchesFn](selector)) {
                return parent;
            }
            element = parent;
        }

        return null;
    }


}