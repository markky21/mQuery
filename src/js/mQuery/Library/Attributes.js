export default {

    attr(key, val) {
        if (key !== undefined && val === undefined) {
            return this.get(0).getAttribute(key);
        } else {
            this.each((node) => {
                node.setAttribute(key, val)
            });
        }
    },

    addClass(className) {

        return this.each(node => {
            if (false) {
                node.classList.add(className);
            } else {
                node.setAttribute('class', node.getAttribute('class') + ' ' + className);
            }
        });

    },

    removeClass(className) {

        return this.each(node => {
            if ('classList' in node) {
                node.classList.remove(className);
            } else {
                let classes = node.getAttribute('class')
                if (classes) {
                    classes = classes.split(" ");
                    const index = classes.indexOf(className);
                    if (index > -1) {
                        classes.splice(index, 1);
                        node.setAttribute('class', classes.join(" "));
                    }
                }
            }
        });

    },

    toggleClass(className, force) {
        return this.each(node => {
            if ('classList' in node) {
                node.classList.toggle(className, force);
            } else {
                let classes = node.getAttribute('class')
                if (classes) {
                    classes = classes.split(" ");
                    const index = classes.indexOf(className);
                    if (index > -1) {
                        classes.splice(index, 1);
                        node.setAttribute('class', classes.join(" "));
                    } else {
                        node.setAttribute('class', node.getAttribute('class') + ' ' + className);
                    }
                }
            }
        });
    },

    hasClass(className) {
        let classes = this.get(0).getAttribute('class')
        if (classes) {
            classes = classes.split(" ");
            return (classes.indexOf(className) > -1);
        } else {
            return false
        }
    },

    css(prop, val) {

        if (typeof prop === 'string' && val === undefined) {
            return window.getComputedStyle(this.get(0))[prop];
        } else if (
            typeof prop === 'string' &&
            (typeof val === 'string' || typeof val === 'number')) {
            this.each((node) => {
                node.style[prop] = val;
            });
            return this;
        } else if (typeof prop === 'object') {
            for (let style in prop) {
                this.css(style, prop[style]);
            }
            return this;
        }

    },

};