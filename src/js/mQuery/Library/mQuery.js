import Attributes from "./Attributes"
import Content from "./Content";
import Events from "./Events";
import Iteration from "./Iteration";
import Selectors from "./Selectors";

import {mixin} from "../Utils/Mixin";


const _NODES = new WeakMap();

class mQuery extends mixin(Attributes, Iteration, Selectors, Content, Events) {

    constructor(nodes) {
        super();

        if (!Array.isArray(nodes)) {
            nodes = [nodes];
        }

        _NODES.set(this, nodes);
    }

    get(index) {
        let nodes = _NODES.get(this);

        if (typeof index === 'number') {
            return nodes[index];
        }
        return nodes;
    }

    static create(nodes) {
        return new mQuery(nodes);
    }

    * [Symbol.iterator]() {
        yield* this.get();
    }
}

export default mQuery;