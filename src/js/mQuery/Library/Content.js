export default {

    text(value) {
        if (value !== undefined) {
            this.each((node) => {
                node.textContent = value;
            });
            return this;
        } else {
            return this.get(0).textContent ;
        }
    },

    html(value) {
        if (value !== undefined) {
            this.each((node) => {
                node.innerHTML = value;
            });
            return this;
        } else {
            return this.get(0).innerHTML;
        }
    }

}