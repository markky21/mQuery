import Ajax from '../Utils/Ajax';


export default {

    get(url = window.location.href, options = {}) {

        return new Ajax('GET', url, options);

    },

    post(url = window.location.href, options = {}) {

        return new Ajax('POST', url, options);

    }

}